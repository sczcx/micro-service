package com.module.admin.prj.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.module.admin.cli.pojo.CliInfo;
import com.module.admin.prj.enums.PrjClientStatus;
import com.module.admin.prj.pojo.PrjInfo;
import com.module.admin.prj.service.PrjClientService;
import com.module.admin.prj.service.PrjInfoService;
import com.module.comm.utils.ClientUtil;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 项目客户端工具类
 * @author yuejing
 * @date 2018年6月19日 上午9:28:30
 */
public class PrjClientUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrjClientUtil.class);

	/**
	 * 发布项目到客户端
	 * @param pi
	 * @param clients
	 * @return
	 */
	private static ResponseFrame release(PrjInfo pi, List<CliInfo> clients) {
		PrjClientService prjClientService = FrameSpringBeanUtil.getBean(PrjClientService.class);
		ResponseFrame frame = new ResponseFrame();
		StringBuffer errorBuffer = new StringBuffer();
		for (CliInfo cliInfo : clients) {
			try {
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				paramsMap.put("prjId", pi.getPrjId());
				paramsMap.put("code", pi.getCode());
				paramsMap.put("name", pi.getName());
				paramsMap.put("version", cliInfo.getVersion());
				//下载路径
				paramsMap.put("pathUrl", cliInfo.getPathUrl());
				//容器类型
				paramsMap.put("container", pi.getContainer());
				//执行的shell命令
				if(FrameStringUtil.isNotEmpty(cliInfo.getShellScript())) {
					paramsMap.put("shellScript", cliInfo.getShellScript());
				} else {
					paramsMap.put("shellScript", pi.getShellScript());
				}
				ResponseFrame clientFrame = ClientUtil.post(cliInfo.getClientId(), cliInfo.getToken(), cliInfo.getIp(), cliInfo.getPort(),
						"/project/release", paramsMap);
				if(ResponseCode.SUCC.getCode() == clientFrame.getCode().intValue()) {
					//修改为发布中
					prjClientService.updateStatus(cliInfo.getClientId(), pi.getPrjId(), cliInfo.getVersion(), PrjClientStatus.ING.getCode(), null);
				} else {
					//处理发布不成功的情况
					errorBuffer.append("客户端[").append(cliInfo.getIp()).append(":").append(cliInfo.getPort()).append("]发布异常; ");
				}
			} catch (Exception e) {
				LOGGER.error("发布到客户端[" + cliInfo.getIp() + ":" + cliInfo.getPort() + "]异常" + e.getMessage(), e);
				errorBuffer.append("客户端[").append(cliInfo.getIp()).append(":").append(cliInfo.getPort()).append("]网络不通; ");
				prjClientService.updateStatus(cliInfo.getClientId(), pi.getPrjId(), cliInfo.getVersion(),
						PrjClientStatus.FAIL.getCode(), "无法访问[" + cliInfo.getIp() + ":" + cliInfo.getPort() + "]，可能网络不通");
			}

		}
		if(errorBuffer.length() == 0) {
			frame.setCode(ResponseCode.SUCC.getCode());
		} else {
			frame.setCode(-404);
			frame.setMessage(errorBuffer.toString());
		}
		return frame;
	}

	/**
	 * 发布项目到所有客户端
	 * @param pi
	 * @param version
	 * @return
	 */
	public static ResponseFrame releaseAll(Integer prjId, String version) {
		PrjClientService prjClientService = FrameSpringBeanUtil.getBean(PrjClientService.class);
		PrjInfoService prjInfoService = FrameSpringBeanUtil.getBean(PrjInfoService.class);
		PrjInfo pi = prjInfoService.get(prjId);
		List<CliInfo> clients = prjClientService.findByPrjId(pi.getPrjId(), version, null);
		return release(pi, clients);
	}

	/**
	 * 发布到单个客户端
	 * @param prjId
	 * @param version
	 * @param clientId
	 * @return
	 */
	public static ResponseFrame releaseSingle(Integer prjId, String version, String clientId) {
		PrjClientService prjClientService = FrameSpringBeanUtil.getBean(PrjClientService.class);
		PrjInfoService prjInfoService = FrameSpringBeanUtil.getBean(PrjInfoService.class);
		PrjInfo pi = prjInfoService.get(prjId);
		List<CliInfo> clients = prjClientService.findByPrjId(pi.getPrjId(), version, clientId);
		return release(pi, clients);
	}
}